package org.oscarehr;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;

import com.google.api.translate.Language;
import com.google.api.translate.Translate;

/**
 * Goal which checks to see what il18n resource keys are missing
 *
 * @goal check
 * 
 * @phase package
 */
public class CheckResources extends AbstractMojo
{
   
	/**
	 * The file all others are checked against
	 * 
	 * @parameter default-value="oscarResources_en.properties"
	 */
	private String referenceResourceFile;
	
	/**
     * My Array.
     *
     * @parameter
     */
    private String[] otherResourceFiles;
    
    /**
    * The project to analyse.
    *
    * @parameter expression="${project}"
    * @required
    * @readonly
    */
    protected MavenProject project; 
    
    
    /**
    * The location of the report.
    *
    * @parameter expression="${project.build.directory}"
    * @required
    */
    private File targetDirectory; 
    
    public void execute() throws MojoExecutionException
    {    	
    	this.getLog().info("CheckResources starting.");
    	this.getLog().info("Using Reference Resource: " + referenceResourceFile);
    	
    	for(String other:otherResourceFiles) {
    		this.getLog().info("Checking Resource: " + other);
    	}
    	
    	File reportDirectory = new File(targetDirectory,"resource-check-reports");
    	if(!reportDirectory.exists()) {
    		if(!reportDirectory.mkdir()) {
    			throw new MojoExecutionException("Unable to create report directory");
    		}
    	}
    	    	
    	//load everything into properties files
    	String resourceDir = project.getBasedir() + File.separator + "src" + File.separator + "main" + File.separator + "resources";    	    	
    	Properties refProps = null;
    	Properties otherProps[] = new Properties[otherResourceFiles.length];
    	PrintWriter outputWriters[] = new PrintWriter[otherResourceFiles.length];
    	try {
    		refProps = loadProperties(resourceDir + File.separator + this.referenceResourceFile);
    		for(int x=0;x<otherProps.length;x++) {
    			otherProps[x] = loadProperties(resourceDir + File.separator + this.otherResourceFiles[x]);
    			outputWriters[x] = new PrintWriter(new FileWriter(new File( reportDirectory, this.otherResourceFiles[x] )),true); 
    		}
    	}catch(IOException e) {
    		getLog().error("Error",e);
    		throw new MojoExecutionException(e.getMessage());
    	}    	    	
    	
    	//do check
    	int totalKeysMissing = 0;
    	Enumeration<Object> e = refProps.keys();
    	while(e.hasMoreElements()) {
    		String key = (String)e.nextElement();
    		//check that this key exists in the other resource files
    		for(int x=0;x<otherProps.length;x++) {
    			if(!otherProps[x].containsKey(key)) {
    				//translate
    				String translatedValue = new String();
    				try {
    					Translate.setHttpReferrer("OSCAR");    			    	
    					translatedValue = Translate.execute(refProps.getProperty(key), getLanguage(referenceResourceFile), getLanguage(otherResourceFiles[x]));
    				}catch(Exception ex) {
    					getLog().warn(ex.getMessage());
    				}
    				outputWriters[x].println(key+ "=" + translatedValue);
    				totalKeysMissing++;
    			}
    		}
    	}
    	//close them
    	for(int x=0;x<outputWriters.length;x++) {
    		outputWriters[x].flush();
    		outputWriters[x].close();
    	}
    	
    	if(totalKeysMissing>0) {
    		getLog().info("Total keys missing: " + totalKeysMissing);
    		getLog().info("Reports located at " + reportDirectory);
    	} else {
    		getLog().info("No keys missing");
    	}
    }
    
    private Properties loadProperties(String filename) throws IOException {
    	getLog().debug("Loading " + filename);
    	Properties props = new Properties();
    	props.load(new FileReader(filename));
    	return props;
    }
    
    private Language getLanguage(String filename){
        if(filename == null){
            return null;
        }else if (filename.equals("oscarResources_en.properties")){
            return Language.ENGLISH;
        }else if (filename.equals("oscarResources_fr.properties")){
            return Language.FRENCH;
        }else if (filename.equals("oscarResources_es.properties")){
            return Language.SPANISH;
        }else if (filename.equals("oscarResources_pt_BR.properties")){
            return Language.PORTUGUESE ;
        }

        return null;
    }
}
