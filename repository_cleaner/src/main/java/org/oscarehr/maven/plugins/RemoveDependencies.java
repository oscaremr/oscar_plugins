package org.oscarehr.maven.plugins;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.versioning.VersionRange;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;

/**
 * Initial intent is to remove org/oscarehr/* from the local maven repository.
 * This is different than PurgeLocalRepository because that removes everything (and excludes the named items).
 * This is almost the reverse in that it only removes what's named.
 * 
 * Just to be absolutely clear... the mvn -U option also prompts to check for updates but as of this writing it still causes problems, it
 * doesn't always update even if you put -U. This can be easily tested by copying a jar in place of one you need, and put a -U in your mvn
 * command, and watch it won't read the original from the source still, it'll try to use your local broken jar only.
 *
 * @goal purge-local-repository-libs
 * @phase pre-clean
 */
public class RemoveDependencies extends AbstractMojo
{
	/**
	 * The local repository, from which to delete artifacts.
	 * 
	 * @parameter default-value="${localRepository}"
	 * @required
	 * @readonly
	 */
	private ArtifactRepository localRepository;

	/**
	 * 
	 * 
	 * @parameter
	 * @required
	 */
	private String[] groupIdsToRemove;

	/**
	 * The projects in the current build. Each of these is subject to
	 * refreshing.
	 * 
	 * @parameter default-value="${reactorProjects}"
	 * @required
	 * @readonly
	 */
	private List<MavenProject> projects;

	/**
	 * The artifact resolver used to re-resolve dependencies, if that option is
	 * enabled.
	 * 
	 * @component
	 */
	private ArtifactResolver resolver;

	/**
	 * Used to construct artifacts for deletion/resolution...
	 * 
	 * @component
	 */
	private ArtifactFactory factory;

	@Override
	public void execute() throws MojoExecutionException
	{
		Log log = getLog();

		for (MavenProject project : projects)
		{
			@SuppressWarnings("unchecked")
			List<Dependency> dependencies = project.getDependencies();
			ArrayList<File> deleteList = new ArrayList<File>();
			ArrayList<Artifact> reResolveList = new ArrayList<Artifact>();
			for (Dependency dependency : dependencies)
			{
				try
				{
					Artifact artifact = factory.createDependencyArtifact(dependency.getGroupId(), dependency.getArtifactId(), VersionRange.createFromVersion(dependency.getVersion()), dependency.getType(), dependency.getClassifier(), dependency.getScope());

					String groupAndArtifactId = dependency.getGroupId() + '.' + dependency.getArtifactId();
					if (isInRemoveList(groupAndArtifactId))
					{
						resolver.resolve(artifact, project.getRemoteArtifactRepositories(), localRepository);

						// /home/tedman/.m2/repository/org/oscarehr/myoscar_server/myoscar_server_client_stubs/SNAPSHOT/myoscar_server_client_stubs-SNAPSHOT.jar
						File file = artifact.getFile();
						File groupAndArtifactIdDirectory = file.getParentFile().getParentFile();

						log.info("Flagging : " + groupAndArtifactIdDirectory.getCanonicalPath());
						deleteList.add(groupAndArtifactIdDirectory);
						reResolveList.add(artifact);
					}
				}
				catch (Exception e)
				{
					log.error("Error", e);
				}
			}

			for (File file : deleteList)
			{
				try
				{
					log.info("Removing : " + file.getCanonicalPath());
					FileUtils.deleteDirectory(file);
				}
				catch (Exception e)
				{
					log.error("Error", e);
				}
			}

			for (Artifact artifact : reResolveList)
			{
				try
				{
					log.info("ReResolving : " + artifact);
					artifact.setResolved(false);
					resolver.resolveAlways(artifact, project.getRemoteArtifactRepositories(), localRepository);
				}
				catch (Exception e)
				{
					log.error("Error", e);
				}
			}
		}
	}

	private boolean isInRemoveList(String groupAndArtifactId)
	{
		for (String s : groupIdsToRemove)
		{
			if (s.equals(groupAndArtifactId)) return(true);
		}

		return(false);
	}
}
