#!/bin/sh

PROJECTS="oscar oscar_maven_repo"
VERSION=0.2-SNAPSHOT
PROJECT_NAME=repository_cleaner_plugin

rm -rf ~/.m2/repository/org/oscarehr/maven/plugins/${PROJECT_NAME}

mvn clean install

INSTALL_PATH_PREFIX=~/.m2/repository/org/oscarehr/maven/plugins/${PROJECT_NAME}/${VERSION}/${PROJECT_NAME}-${VERSION}

for foo in ${PROJECTS};
do
	mvn install:install-file -Dpackaging=jar -DcreateChecksum=true -DlocalRepositoryPath=../../${foo}/local_repo -DlocalRepositoryId=local_repo -DgroupId=org.oscarehr.maven.plugins -DartifactId=${PROJECT_NAME} -Dversion=${VERSION} -Dfile=${INSTALL_PATH_PREFIX}.jar -DpomFile=${INSTALL_PATH_PREFIX}.pom
done;

