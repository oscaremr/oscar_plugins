DEPRECATED : all projects use fully qualified version numbers now so no more need to delete/purge repository libraries.

------------
Requirements (not really requirements but it's what I used in the initial development of this code)
------------
- jdk 1.6.0_24
- maven 2.2.1

------------------
Repository Cleaner
------------------
This plugin was built because when you have a maven repository and you have libraries, those libraries are only checked once a day (or something like that). It copied them to a personas local-repository which means during development if the library changes, maven won't pick up the change and it will cause a build failure. Even if you use mvn -U and SNAPSHOT, it does not work. The's roughly 2 work arounds, 1) mvn install on the local machine to replace the jar in the local-repository or 2) remove the jars from the local repository to ensure it re-reads it.

This plugin allows a project to specify which libraries to delete upon a mvn clean, i.e. org.oscarehr.* that way we always check for updates. We didn't want to use the maven dependency purger because that one removes all dependencies which means all the org.apache.* stuff would be re-downloaded each time which is long and painful.

The default phase for this is "clean", and the default goal for this is "purge-local-repository-libs".

---------------------
Example Configuration
---------------------
<plugin>
	<groupId>org.oscarehr.maven.plugins</groupId>
	<artifactId>repository_cleaner_plugin</artifactId>
	<version>SNAPSHOT</version>
	<configuration>
		<groupIdsToRemove>
			<param>org.oscarehr.myoscar_server</param>
			<param>org.oscarehr.utils</param>
		</groupIdsToRemove>
	</configuration>
	<executions>
		<execution>
			<goals>
				<goal>purge-local-repository-libs</goal>
			</goals>
		</execution>
	</executions>
</plugin>